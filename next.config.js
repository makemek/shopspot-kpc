/**
 * NEXTJS DEFAULT WEBPACK PLUGIN
 *
 * PagesPlugin
 * DynamicChunksPlugin
 * CaseSensitivePathPlugin
 * json-loader
 * emit-file-loader
 * babel-loader
 *
 *
 * DEV
 * -------
 * webpack.optimize.CommonsChunkPlugin -- consider all node_modules as common chunks
 * HotModuleReplacementPlugin
 * NoEmitOnErrorsPlugin
 * UnlinkFilePlugin
 * cheap-module-inline-source-map
 * hot-self-accept-loader
 * react-hot-loader/webpack
 *
 *
 * PROD
 * ------
 * webpack.optimize.CommonsChunkPlugin -- Move modules used in at-least 1/2 of the total pages into commons.
 * CombineAssetsPlugin
 * webpack.optimize.UglifyJsPlugin
 */

/**
 * BABEL
 *
 * PRESET
 * babel-preset-env
 * babel-preset-react
 *
 * PLUGINS
 * babel-plugin-react-require
 * ./plugins/handle-import
 * babel-plugin-transform-object-rest-spread
 * babel-plugin-transform-class-properties
 * babel-plugin-transform-runtime
 * styled-jsx/
 *
 * DEV
 * ------
 * babel-plugin-transform-react-jsx-source
 *
 * PROD
 * ------
 * babel-plugin-transform-react-remove-prop-types
 */

// This file is not going through babel transformation.
// So, we write it in vanilla JS
// (But you could use ES2015 features supported by your Node.js version)

let customConfig = {
  module: {
    rules: []
  },
  plugins: []
}

switch(process.env.NODE_ENV) {

  case 'production': return customConfig
  case 'beta': return customConfig
  case 'staging': return customConfig

  default: return customConfig
}

module.exports = {
  webpack: (config, { dev }) => {
    console.log(config)

    // Perform customizations to webpack config
    config.modules.rules.concat(customConfig.module.rules)
    config.plugins.concat(customConfig.plugins)

    // Important: return the modified config
    return config
  },
  webpackDevMiddleware: (config) => {
    // Perform customizations to webpack dev middleware config

    // Important: return the modified config
    return config
  }
}
